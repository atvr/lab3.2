<?php

namespace application\controller;

require_once("application/view/View.php");
require_once("login/controller/LoginController.php");
require_once 'login/controller/RegistrationController.php';
require_once 'login/model/RegistrationModel.php';



class Application {
	private $view;

	private $loginController;
	private $regController;
	
	public function __construct() {
		$loginView = new \login\view\LoginView();
		$regView = new \login\view\RegistrationView();
		
		$this->loginController = new \login\controller\LoginController($loginView);
		$this->regController = new \login\controller\RegistrationController($regView);
		$this->view = new \application\view\View($loginView,$regView);
	}
	
	public function doFrontPage() {
		$this->loginController->doToggleLogin();
	
		if ($this->loginController->isLoggedIn()) {
			$loggedInUserCredentials = $this->loginController->getLoggedInUser();
			return $this->view->getLoggedInPage($loggedInUserCredentials);	
		} else {
			if($this->regController->doesUserWantToRegister()) {
				if($this->regController->isUserSendingRegistrationData()) {
					$regError = $this->regController->processRegistrationInformationAndReturnResult();
					if(!$regError) {
						return $this->view->getLoggedOutPageRegSuccess();
					}
				}
				
				return $this->view->getRegistrationPage();
			}
			else {
				return $this->view->getLoggedOutPage();	
			}
		}
	}
}
