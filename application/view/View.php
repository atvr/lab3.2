<?php

namespace application\view;

require_once("common/view/Page.php");
require_once("SwedishDateTimeView.php");
require_once 'login/view/RegistrationView.php';


class View {
	private $loginView;

	private $timeView;
	
	private $regView;
	
	public function __construct(\login\view\LoginView $loginView, \login\view\RegistrationView $regView) {
		$this->loginView = $loginView;
		$this->timeView = new SwedishDateTimeView();
		$this->regView = $regView;
	}
	
	public function getLoggedOutPage() {
		$html = $this->getHeader(false);
		$loginBox = $this->loginView->getLoginBox(); 
		$regLink = $this->loginView->getRegisterLink();
		$html .= $regLink;
		$html .= "<h2>Ej Inloggad</h2>
				  	$loginBox
				 ";
		$html .= $this->getFooter();

		return new \common\view\Page("Laboration. Inte inloggad", $html);
	}
	
	public function getLoggedOutPageRegSuccess() {
		$html = $this->getHeader(false);
		$loginBox = $this->loginView->getLoginBoxRegSuccess();
		$regLink = $this->loginView->getRegisterLink();
		$html .= $regLink;
		$html .= "<h2>Ej Inloggad</h2>
				  	$loginBox
				 ";
		$html .= $this->getFooter();

		return new \common\view\Page("Laboration. Inte inloggad", $html);
	}
	
	public function getRegistrationPage() {
		$header = $this->getHeader(FALSE);
		$regbox = $this->regView->getRegistrationBox();
		$backLink = $this->regView->getGoBackLink();
		$content =  "<h2>Ej Inloggad, Registrerar användare</h2>
				  	$regbox
				 ";
		$footer = $this->getFooter();
		$html = $header . $backLink . $content . $footer;
		//return $html;
		return new \common\view\Page("Laboration. Registrerar invändare", $html);
	}
	
	
	
	public function getLoggedInPage(\login\model\UserCredentials $user) {
		$html = $this->getHeader(true);
		$logoutButton = $this->loginView->getLogoutButton(); 
		$userName = $user->getUserName();

		$html .= "
				<h2>$userName är inloggad</h2>
				 	$logoutButton
				 ";
		$html .= $this->getFooter();

		return new \common\view\Page("Laboration. Inloggad", $html);
	}
	
	
	
	private function getHeader($isLoggedIn) {
		$ret =  "<h1>Laborationskod xx222aa</h1>";
		return $ret;
		
	}

	private function getFooter() {
		$timeString = $this->timeView->getTimeString(time());
		return "<p>$timeString<p>";
	}
	
	
}
