<?php

namespace login\model;


require_once("UserCredentials.php");
require_once("common/model/PHPFileStorage.php");

class UserList {
	private $adminFile;

	private $users;

	public function  __construct( ) {
		$this->users = array();
		
		$this->loadAdmin();
	}

	public function findUser(UserCredentials $fromClient) {
		foreach($this->users as $user) {
			if ($user->isSame($fromClient) ) {
				\Debug::log("found User");
				return  $user;
			}
		}
		throw new \Exception("could not login, no matching user");
	}

	public function update(UserCredentials $changedUser) {
		$this->adminFile->writeItem($changedUser->getUserName(), $changedUser->toString());

		\Debug::log("wrote changed user to file", true, $changedUser);
		$this->users[$changedUser->getUserName()->__toString()] = $changedUser;
	}


	private function loadAdmin() {
		$this->adminFile = new \common\model\PHPFileStorage("data/admin.php");
		$allUsers = $this->adminFile->getAllUsers();
		for($i = 0; $i < count($allUsers); $i++) {
		
		$userString = $this->adminFile->readItem($allUsers[$i]);
		$someUser = UserCredentials::fromString($userString);

		array_push($this->users, $someUser);
		}
	}


	/*
	private function loadAdmin() {
		
		$this->adminFile = new \common\model\PHPFileStorage("data/admin.php");
		try {
			$adminUserString = $this->adminFile->readItem("Admin"); //funkar med Per istället för Admin. fixa denna så är det nog klart...
			var_dump($adminUserString);
			$admin = UserCredentials::fromString($adminUserString);
		} catch (\Exception $e) {
			\Debug::log("Could not read file, creating new one", true, $e);

			$userName = new UserName("Admin");
			$password = Password::fromCleartext("Password");
			$admin = UserCredentials::create( $userName, $password);
			$this->update($admin);
		}

		$this->users[$admin->getUserName()->__toString()] = $admin;
	}*/
	
	
	
	
	public function createNewUser($usernameString, $passwordString) {
		$userName = new UserName($usernameString);
		$password = Password::fromCleartext($passwordString);
		$newUser = UserCredentials::create($userName, $password);
		$this->update($newUser);
	}
	
	private function getListOfUsernames() {
		return $this->adminFile->getAllUsers();
	}
	
	function doesUserExist($usernameString) {
		$users = $this->getListOfUsernames();
		if(in_array($usernameString, $users)) {
			return TRUE;
		}
		else return FALSE;
	}
}