<?php

	namespace login\model;
	include_once 'login/model/UserName.php';
	include_once 'login/model/Password.php';
	include_once 'login/model/UserCredentials.php';
	include_once 'login/model/UserList.php';
	include_once 'login/model/RegistrationError.php';
	include_once 'login/model/RegistrationData.php';
	include_once 'common/model/PHPFileStorage.php';
	
	class RegistrationModel   {
		
		private $userList;
		private $unallowedCharsArr;
		private static $unallowedChars = "<>()=*-_?!\"\'";
		
		function __construct() {
			$this->userList = new UserList();
			$this->initUnallowedChars();	
		}
		
		private function initUnallowedChars() {
			$this->unallowedCharsArr = array();
			for($i = 0; $i < strlen(self::$unallowedChars); $i++) {
				array_push($this->unallowedCharsArr, self::$unallowedChars[$i]);
			}

		}
		
		//@return bool FALSE eller RegistrationError.
		public function IsRegistrationDataIncorrect(RegistrationData $regData) {
				
			$username = '';
			$password = '';
			

			$regError = new RegistrationError();
			
			$correct = TRUE;
			
			try {
				$username = new UserName($regData->Username);
			}
			catch(\Exception $e) {
				$correct = FALSE;
				$lenUsername = strlen($regData->Username);
				if($lenUsername === 0) {
					$regError->usernameMissing = TRUE;
				}
				elseif($lenUsername > UserName::MAXIMUM_USERNAME_LENGTH) {
					$regError->usernameTooLong = TRUE;
				}
				elseif($lenUsername < UserName::MINIMUM_USERNAME_LENGTH) {
					$regError->usernameTooShort = TRUE;
				}
			}
			if($correct) {
				if($this->userList->doesUserExist($regData->Username)) {
					$regError->usernameAlreadyExist = TRUE;
				}
			}
			try {
				$password = Password::fromCleartext($regData->Password);
			}
			catch(\Exception $e) {
				$correct = FALSE;
				$lenPassword = strlen($regData->Password);
				if($lenPassword === 0) {
					$regError->passMissing = TRUE;
				}
				elseif($lenPassword > Password::MAXIMUM_PASSWORD_CHARACTERS) {
					$regError->passTooLong = TRUE;
				}
				elseif($lenPassword < Password::MINIMUM_PASSWORD_CHARACTERS) {
					$regError->passTooShort = TRUE;
				}
			}
			
			if($regData->Password !== $regData->Password2) {
				$correct = FALSE;
				$regError->passesDoNotMatch = TRUE;
			}
			
			if($this->containsUnallowedChars($regData->Username)){
				$correct = FALSE;
				$regError->usernameContainsUnallowedChar = TRUE;
			}
			
			if($this->containsUnallowedChars($regData->Password)){
				$correct = FALSE;
				$regError->passwordContainsUnallowedChar = TRUE;
			}
			
			
			if($correct) {
				return FALSE;
			}
			else return $regError;
		}

		public function registerUser(RegistrationData $data) {
			$this->userList->createNewUser($data->Username, $data->Password);
		}
		
		private function containsUnallowedChars($someString) {
			for($i = 0; $i < strlen($someString); $i++) {
				if(in_array($someString[$i], $this->unallowedCharsArr)) {
					return TRUE;
				}
			}
			return FALSE;
		}
		
		
		
		
		
		
		
		
	}

?>