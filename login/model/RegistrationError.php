<?php

	namespace login\model;
	
	class RegistrationError{
		
		public $usernameMissing;
		public $usernameTooLong;
		public $usernameTooShort;
		public $passMissing;
		public $usernameAlreadyExist;
		public $passTooLong;
		public $passTooShort;
		public $passesDoNotMatch;
		public $usernameContainsUnallowedChar;
		public $passwordContainsUnallowedChar;
		
		function __construct() {
		}
	}


?>