<?php
	
	namespace login\controller;
	
	require_once 'login/view/RegistrationView.php';
	require_once 'login/model/RegistrationModel.php';
	require_once 'login/model/RegistrationData.php';
	
	
	class RegistrationController{
		
		private $regView;
		private $regMod;
		
		function __construct(\login\view\RegistrationView $regView) {
			$this->regView = $regView;
			$this->regMod = new \login\model\RegistrationModel();
		}
		
		public function doesUserWantToRegister(){
			return $this->regView->doesUserWantToRegister();
		}
	
		public function isUserSendingRegistrationData() {
			return $this->regView->isUserSendingRegistrationData();
		}
		
		//@var bool registrering lyckad eller inte.
		public function processRegistrationInformationAndReturnResult() {
			$data = $this->regView->getRegistrationData();
			$err = $this->regMod->IsRegistrationDataIncorrect($data);
			if(!$err) {
				$this->regMod->registerUser($data);
				return FALSE;
			}
			else {
				$this->regView->setErrorMsg($err);
				return TRUE;
			}
		}
		
		
		
		
	}

?>