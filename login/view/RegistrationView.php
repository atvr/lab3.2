<?php

namespace login\view;

	require_once 'login/model/RegistrationData.php';
	require_once 'login/model/RegistrationError.php';
	require_once 'login/model/UserName.php';
	require_once 'login/model/Password.php';
	
	
	class RegistrationView {
		
		private static $REG_USERNAME = "RegistrationView::Username";
		private static $REG_PASSWORD = "RegistrationView::Password";
		private static $REG_PASSWORD2 = "RegistrationView::Password2";
		
		private static $MAINPAGELINK = "index.php";
		private static $REGISTER = "register";
		
		private $errMsg;
		
		function __construct() {
			$this->errMsg = null;
		}
		
		public function getGoBackLink() {
		return "<p><a href='". self::$MAINPAGELINK . "'>Tillbaka</a></p>";
		}
		
		public function getRegistrationBox() {
			$html = "
			<form action='?" . self::$REGISTER . "' method='post' enctype='multipart/form-data'>
				<fieldset>
					$this->errMsg
					<legend>Registrera ny användare - Skriv in användarnamn och lösenord</legend>
					<p><label for='UserNameID' >Användarnamn :</label>
					<input type='text' size='20' name='" . self::$REG_USERNAME . "' id='UserNameID' value='' /></p>
					<p><label for='PasswordID' >Lösenord  :</label>
					<input type='password' size='20' name='" . self::$REG_PASSWORD . "' id='PasswordID' value='' /></p>
					<p><label for='PasswordID2' >Repetera Lösenord  :</label>
					<input type='password' size='20' name='" . self::$REG_PASSWORD2 . "' id='PasswordID2' value='' /></p>
					<p><input type='submit' name=''  value='Registrera' /></p>
				</fieldset>
			</form>";
			
		return $html;
		}
		
		
		
		private function getRegPassword() {
		return $_POST[self::$REG_PASSWORD];
		}
	
		private function getRegPassword2() {
			return $_POST[self::$REG_PASSWORD2];		
		}
		
		private function getRegUsername() {
			return $_POST[self::$REG_USERNAME];
		}
		
		public function isUserSendingRegistrationData() {
			$regUsername = $this->getRegUsername();
			$regPass = $this->getRegPassword();
			$regPass2 = $this->getRegPassword2();
			
				if($regUsername ==! null) {
					return TRUE;
				}
				return FALSE;
		}
		
		public function getRegistrationData() {
			$regUsername = $this->getRegUsername();
			$regPass = $this->getRegPassword();
			$regPass2 = $this->getRegPassword2();
			$regData = new \login\model\RegistrationData($regUsername,$regPass,$regPass2);
			return $regData;
		}
		
		public function doesUserWantToRegister() {
			if($_SERVER['QUERY_STRING'] != self::$REGISTER) {
				return FALSE;
			}
			return TRUE;
		}

		public function setErrorMsg(\login\model\RegistrationError $err) {
			$msg = '';
			if($err->usernameMissing) {
				$msg .= "Användarnamn saknas.<br />";
			}
			if($err->usernameTooLong){
				$maxlen = \login\model\UserName::MAXIMUM_USERNAME_LENGTH;//
				$msg .= "Användarnamnet är för långt. Högst $maxlen tecken.<br />";
			}
			if($err->usernameTooShort){
				$minlen = \login\model\UserName::MINIMUM_USERNAME_LENGTH;
				$msg .= "Användarnamn är för kort. Minst $minlen tecken.<br />";
			}
			if($err->passMissing){
					$msg .= "Lösenord saknas.<br />";
			}
			if($err->usernameAlreadyExist){
				$msg .= "Användarnamn existerar redan.<br />";
			}
			if($err->passTooLong){
				 $maxlen = \login\model\Password::MAXIMUM_PASSWORD_CHARACTERS;
				$msg .= "Användarnamnet är för långt. Högst $maxlen tecken.<br />";
			}
			if($err->passTooShort){
				$minlen = \login\model\Password::MINIMUM_PASSWORD_CHARACTERS ;
				$msg .= "Användarnamnet är för kort. Minst $minlen tecken.<br />";
			}
			if($err->passesDoNotMatch){
				$msg .= "Lösenorden matchar inte.<br />";
			}
			if($err->usernameContainsUnallowedChar) {
				$msg .= "Användarnamnet innehåller ogiltiga tecken.<br />";
			}
			if($err->passwordContainsUnallowedChar) {
				$msg .= "Lösenordet innehåller ogiltiga tecken.<br />";
			}
			
			if($msg !== "") {
				$this->errMsg =  "<p>" . $msg . "</p>";
			}	
		}
			
		
	}

?>